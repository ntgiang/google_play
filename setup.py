#!/usr/bin/env python

from setuptools import setup

setup(
    name='google_play',
    author='Igor Skrynkovskyy',
    maintainer='David Nguyen',
    description='Google Play app fetcher',
    license="MIT",
    url='https://bitbucket.org/ntgiang/google_play',
    version='1.0.2',
    packages=['google_play'],
    test_suite='tests',
    install_requires=(
        'Flask',
        'beautifulsoup4==4.3.2',
        'lxml',
        'requests==2.2.1',
        'protobuf'
    )
)
