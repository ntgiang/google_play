# !/usr/bin/python
import sys
import os.path
import urllib

import re
from bs4 import BeautifulSoup
import requests

# Do not remove
GOOGLE_LOGIN = GOOGLE_PASSWORD = AUTH_TOKEN = None

from flask import current_app
from googleplay import GooglePlayAPI
from helpers import sizeof_fmt


CATEGORIES = [
    "application", "app_wallpaper", "app_widgets", "arcade",
    "books_and_reference", "brain", "business", "cards",
    "casual", "comics", "communication", "education",
    "entertainment", "finance", "game", "game_wallpaper",
    "game_widgets", "health_and_fitness", "libraries_and_demo", "lifestyle",
    "media_and_video", "medical", "music_and_audio", "news_and_magazines",
    "personalization", "photography", "productivity", "racing",
    "shopping", "social", "sports", "sports_games",
    "tools", "transportation", "travel_and_local", "weather"
]

FREE = 'topselling_free'
PAID = 'topselling_paid'


def _get_apps(url):
    r = requests.get(url)
    if r.status_code != 200:
        return None

    apps = []
    soup = BeautifulSoup(r.content, "lxml")
    for elem in soup.find_all('div', 'card'):
        apps.append(elem.attrs['data-docid'])

    return apps


def leaderboard(identifier, category=None, start=0,
                num=24, hl="vn", gl="vn"):
    if identifier not in ('topselling_paid', 'topselling_free'):
        raise Exception("identifier must be topselling_paid or topselling_free")

    url = 'https://play.google.com/store/apps'
    if category:
        if category not in CATEGORIES:
            raise Exception('%s not exists in category list' % category)
        url += "/category/" + str(category).upper()

    url += "/collection/%s?start=%s&num=%s&hl=%s&gl=%s" % (identifier, start, num, hl, gl)

    return _get_apps(url)


def search(query, start=0, num=24, hl="vn", gl="us"):
    url = ('https://play.google.com/store/search'
           '?q=%s&start=%s&num=%s&hl=%s&gl=%s') % (query, start, num, hl, gl)

    return _get_apps(url)


def developer(developer, start=0, num=24, hl="vn", gl="us"):
    url = ('https://play.google.com/store/apps/developer'
           '?id=%s&start=%s&num=%s&hl=%s&gl=%s') % (urllib.quote_plus(developer), start, num, hl, gl)

    return _get_apps(url)


def app(package_name, hl='vn', gl="vn"):
    package_url = ("https://play.google.com/store/apps/details"
                   "?id=%s&hl=%s&gl=%s") % (package_name, hl, gl)

    r = requests.get(package_url)
    if r.status_code != 200:
        return None

    soup = BeautifulSoup(r.content, "lxml")

    app = dict()
    app['title'] = soup.find('div', 'id-app-title').text.strip()
    app['url'] = package_url
    app['package_name'] = package_name
    try:
        app['description'] = soup.find('div', 'show-more-content').text.strip()
    except:
        app['description'] = ''
    try:
        app['html_description'] = unicode(soup.find('div', 'show-more-content'))
    except:
        app['html_description'] = ''
    try:
        app['category'] = soup.find('span', itemprop='genre').text
    except:
        app['category'] = ''
    app['video_url'] = ""
    player_container = soup.find('span', class_="play-action-container")
    if player_container:
        app['video_url'] = player_container.attrs['data-video-url']
    try:
        app['logo'] = soup.find('img', "cover-image").attrs['src']
    except:
        app['logo'] = ''
    try:
        app['price'] = soup.find('meta', itemprop="price").attrs['content']
    except:
        pass
    try:
        app['developer_name'] = soup.find('div', itemprop="author").a.text.strip()
    except:
        app['developer_name'] = ''
    try:
        app['content_rating'] = soup.find('div', itemprop="contentRating").text.strip()
    except:
        pass
    try:
        app['developer_email'] = soup.find('a', href=re.compile("^mailto")).attrs['href'][7:]
    except:
        app['developer_email'] = ''
    try:
        link = soup.find('a', "dev-link").attrs['href']
        developer_website = re.search('\?q=(.*)&sa', link)
        if developer_website:
            app['developer_website'] = developer_website.group(1) or ''
        else:
            app['developer_website'] = ''
    except:
        app['developer_website'] = ''
    try:
        app['rating'] = float(soup.find('div', 'score').text.replace(',', '.'))
    except:
        app['rating'] = 0

    try:
        app['reviews'] = int(soup.find('span', 'reviews-num').text.replace('.', ''))
    except:
        app['reviews'] = 0

    try:
        app['version'] = soup.find('div', itemprop="softwareVersion").text.strip()
    except:
        app['version'] = "1.0.0"

    try:
        app['size'] = soup.find('div', itemprop="fileSize").text.strip()
    except:
        app['size'] = 0

    try:
        app['installs'] = soup.find('div', itemprop="numDownloads").text.strip()
    except:
        app['installs'] = ''
    try:
        app['android'] = soup.find('div', itemprop="operatingSystems").text.strip()
    except:
        app['android'] = ''
    try:
        app['datePublished'] = soup.find('div', itemprop="datePublished").text.strip()
    except:
        app['datePublished'] = ''
    app['screenshots'] = [im.attrs['src']
                          for im in soup.find_all('img', class_="screenshot")]

    app['full_screenshots'] = [im.attrs['src']
                               for im in soup.find_all('img', class_="full-screenshot")]

    html = soup.find('div', "rec-cluster")
    if html:
        app['similar'] = [similar.attrs['data-docid']
                          for similar in html.find_all('div', 'card')]
    else:
        app['similar'] = []

    return app


def download(package_name):
    ANDROID_ID = os.environ.get("GP_ANDROID_ID") or current_app.config.get("GP_ANDROID_ID", "")  # "xxxxxxxxxxxxxxxx"
    GOOGLE_LOGIN = os.environ.get("GP_LOGIN") or current_app.config.get("GP_LOGIN", "")  # "username@gmail.com"
    GOOGLE_PASSWORD = os.environ.get("GP_PASSWORD") or current_app.config.get("GP_PASSWORD", "")
    AUTH_TOKEN = None  # "yyyyyyyyy"
    DOWNLOAD_PATH = os.environ.get("GP_DOWNLOAD_PATH") or current_app.config.get("GP_DOWNLOAD_PATH", "")

    # force the user to edit this file
    if any([each is None for each in [ANDROID_ID, GOOGLE_LOGIN, GOOGLE_PASSWORD]]):
        raise Exception("config.py not updated")

    if not package_name:
        print "Package name not valid"
        sys.exit(0)
    filename = package_name + ".apk"

    if not os.path.exists(DOWNLOAD_PATH):
        os.makedirs(DOWNLOAD_PATH)

    completeName = os.path.join(DOWNLOAD_PATH, filename)

    # Connect
    api = GooglePlayAPI(ANDROID_ID)
    api.login(GOOGLE_LOGIN, GOOGLE_PASSWORD, AUTH_TOKEN)

    # Get the version code and the offer type from the app details
    m = api.details(package_name)
    doc = m.docV2

    # Check if file exists
    if os.path.isfile(completeName):
        apk_size = os.stat(completeName).st_size
        if int(apk_size) == int(doc.details.appDetails.installationSize):
            print "%s downloaded is newest" % package_name
    else:
        vc = doc.details.appDetails.versionCode
        ot = doc.offer[0].offerType

        # Download
        print "Downloading %s package %s" % (sizeof_fmt(doc.details.appDetails.installationSize), package_name)
        data = api.download(package_name, vc, ot)
        open(completeName, "wb").write(data)
        print " Done"
    return doc.details.appDetails
